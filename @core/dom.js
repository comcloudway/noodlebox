import {Stack} from './hooks.js';
import {StyleSheet} from './css.js';

/**
 * A Function used to rebind HTMLElement rendering
 * Handles automatic rerendering of Elements once dependencies change
 * @param {Function<HTMLElement>} render - Callback called to render an element, also used to rerender element
 * @param {Array} depends - Dependency Array, see hooks.bind()
 * @returns {HTMLElement}
 */
const _ = (render=()=>{}, depends=[]) => {
  let el = render() || document.createElement('div');

  let dependencies = depends.map(dep=>dep(()=>{
    let n = render() || document.createElement('div');
    if (n === false) return;

    el.removeEventListener('DOMNodeRemoved', list);
    el.replaceWith(n);
    el.remove();
    let listener = n.addEventListener('DOMNodeRemoved', e=>{
      if (e.target !== el || !e.relatedNode.isConnected) return;
      //      dependencies.forEach(d=>d());
      n.removeEventListener('DOMNodeRemoved', listener);
    });
    el=n;
  }));

  let list = el.addEventListener('DOMNodeRemoved', e=>{
    if (e.target !== el || !e.relatedNode.isConnected) return;
    //  dependencies.forEach(d=>d());
    el.removeEventListener('DOMNodeRemoved', list);
  })
  return el;
};
/**
 * Javascript DOM Element creation made easy
 * NOTE: use opts.tag to specify the element's tag name
 * @param {Object} opts - Element Properties
 * @returns {HTMLElement}
 */
_.Element = (opts={}) => {
  let tag = opts.tag;
  delete opts.tag;

  let elem = document.createElement(tag);

  Object.entries(opts).forEach(([k,v])=>{
    try {
      elem[k]=v
    } catch(_) {
    }
  });

  let list = elem.addEventListener('DOMNodeRemoved', e=>{
    if (e.target !== elem || !e.relatedNode.isConnected) return;
    elem.removeEventListener('DOMNodeRemoved', list);
    if (!!elem.onremove) elem.onremove();
  })

  return elem;
};

export class Component extends HTMLElement {
  constructor() {
    super();
    this._shadow = this.attachShadow({mode: 'open'});
    this._target = this;
    this._sheets=[];
    let list = this.addEventListener('DOMNodeRemoved', e=>{
      if (e.target !== this || !e.relatedNode.isConnected) return;
      this.removeEventListener('DOMNodeRemoved', list);
      this.unregister();
    });
  };
  StyleSheet(a,b,c,d) {
    return StyleSheet(a,b,c,d, this._shadow);
  }
  unregister() {
    Object.values(this._target.children||{}).forEach(c=>c.remove());
    if (this._css) this._css.remove();
    if (this._css2) this._css2.remove();
    this._sheets.forEach(sh=>{
      this._target.classList.toggle(sh.className);
      sh.remove();
    })
  }

  set style(value) {
    if (!!this._css) {
      this._target.classList.toggle(this._css.className)
      this._css.remove();
    }
    this._css = this.StyleSheet(value);
    this._target.classList.toggle(this._css.className)
  }
  set custom_style(value) {
    if (!!this._css2) {
      this.classList.toggle(this._css2.className)
      this._css2.remove();
    }
    this._css2 = StyleSheet(value,undefined,undefined,'{CSS}',this);
    this.classList.toggle(this._css2.className)
    this.setAttribute('style',this._css2.content);
  }

  set stylesheets(s) {
    this._sheets.forEach(sh=>{
      this._target.classList.toggle(sh.className);
      sh.remove();
    });
    this._sheets=s.map(us=>this.StyleSheet(...us));
    this._sheets.forEach(sh=>{
      this._target.classList.toggle(sh.className);
    })
  }

  set children(value=[]) {
    if (this._target.children.length>0) this._target.children.forEach(c=>c.remove());
    this._target.append(...value)
  }
  append(...items) {
    this._target.append(...items)
  }
  appendChild(item){
    this._target.appendChild(item)
  }

  static define(id='', class_definition=Component) {
    let d = class_definition;
    if (!customElements.get(id)) customElements.define(id, d);
    return (props={}) => {
      let el = _.Element({
        ...props,
        tag: id
      });
      return el
    }
  }
}
customElements.define('noodlebox-component', Component);
_.Component=Component;

export default _
