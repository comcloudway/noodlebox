/**
 * A value stack used to store values
 * @constructor {any} initial - default value
 */
export class Stack {
  constructor(initial, update_callback=()=>true) {
    this._state = initial;
  }

  get current() {
    return this._state;	
  }

  set current(value) {
    if (update_callback(value)) this._state = value;
  }
}

/**
 * basic use state hook
 * used to dynamically store & update values
 * NOTE: for objects use complex hook
 * @param {String | Boolean | Number | Array} initial - default value
 * @returns {[Object, Function]} Object{current, bind, subscribe}, Function(cb=>cb)
 */
export const use_state = (initial) => {
  let state = new Stack(initial);
  let listeners = {};

  const listen = cb => {
    let key = Object.keys(listeners).length;
    listeners[key]=cb;
    return ()=>{
      delete listeners[key];
    };
  };

  let socket = [];
  const set = cb => {
    let id = socket.length;
    socket[id] = new Promise(async res => {
      let x = state.current;
      if (id !== 0) x = await socket[id - 1];
      let r = cb(x);
      let old = state.current;
      state._state=r;
      if (JSON.stringify(old)!==JSON.stringify(r)) {
        Object.entries(listeners).forEach(([_, cn])=>{
          cn(r)
        });
      }
      res(r)
    })
  };

  return [{
    get current() {
      return state.current;
    },
    subscribe: listen,
    bind: ()=>{
      return (cb=()=>{})=>{
        let list = listen(()=>{
          cb();
        });
        return ()=>list();
      };
    }

  }, set];
};

const deep_copy = (object={}) => {
  let out = Array.isArray(object)?[]:{};

  if (typeof object !== 'object' || !object) {
    return object;
  }

  Object.entries(object).forEach(([k, v])=>out[k]=deep_copy(v));

  return out;
}
/**
 * complex use state hook
 * used to dynamically store & update values
 * @param {Object} initial - default value
 * @returns {[Object, Function]} Object{current, bind, subscribe}, Function(cb=>cb)
 */
export const use_complex_state = initial => {
  let state = new Stack(deep_copy(initial));
  let listeners = {};

  let listen = (cb=()=>{}, targets=[]) => {
    if (!Array.isArray(targets)) {
      throw new Error('targets is not an array');
    }
    let k = Object.keys(listeners || {}).length;
    listeners[k]={
      target: targets,
      call: cb
    };
    return ()=>{
      delete listeners[k]
    }
  };

  let update = (old={}, ov={}) => {
    let changes = {};

    let comp = (b, a, prefix=[]) => {
      Object.entries(a || {}).forEach(([lv1_key, lv1_dt])=>{
        if (typeof lv1_dt === 'object' && !Array.isArray(lv1_dt)) {
          comp(b[lv1_key] || {},lv1_dt,[...prefix, lv1_key]);
        } else {
          let p = [...prefix, lv1_key];
          if (typeof lv1_dt !== typeof b[lv1_key] 
            || (Array.isArray(lv1_dt) && !Array.isArray(b[lv1_key])) 
            || (!Array.isArray(lv1_dt) && Array.isArray(b[lv1_key]))
            || lv1_dt !== b[lv1_key]
            // somewhat still have to compare old/missing keys (deleting sth leads to it not being checked)
          ) {
            // not the same
            let path = [...p];
            for (let i = 0; i<=path.length+1; i++) {
              changes[path.join('.')]=true;
              path.pop();
            }
          }
        }
      });
    }
    comp(old, ov);
    comp(ov, old);

    changes = Object.keys(changes);
    Object.values(listeners).forEach(async ({target=[], call=()=>{}})=>{

      if (target.length===0||(target.length===1&&target[0]==='')) {
        call(ov, target, old);
      } else {
        target.forEach(t=>{
          if(changes.includes(t)) call(ov, [t], old);
        });
      }
    });
  };

  let socket = [];
  let set = cb => {
    let id = socket.length;
		socket[id] = new Promise(async res => {
			let x = state.current;
			if (id !== 0) x = await socket[id - 1];
			x = deep_copy(x);
			let r = cb(x);
			if (typeof r !== "object" || Array.isArray(r)) return;
			let old = deep_copy(state.current);
			state._state=deep_copy(r);
			update(old, r);
			res(state._state)
		});
  };

  return [{
    get current() {
      return state.current;
    },
    subscribe: listen,
		bind: (...targets)=>{
			let deps = [];
			if(targets.length===1 && Array.isArray(targets[0])) {
				deps=targets[0];
			} else {
				deps=targets;
			}
			return (cb=()=>{})=>{
				let list = listen(()=>{
					cb();
				}, deps);
				return ()=>list();
			};
		}
  }, set];
};

/**
 * use effect hook
 * used to run a function once a dependency updates
 * @param {Function} callback - callback to call on update
 * @param {Array} dependencies - Dependency Array (see use_state or use_complex_state .bind)
 * @returns {Function} Function to call to unsubscribe listener
 */
export const use_effect = (callback=()=>{}, dependencies=[]) => {
	let clear = callback();
	let subs = dependencies.map(dep=>dep(()=>{
		// clearup old effects
		clear();
		// run new
		clear = callback();
	}));
	return () => {
		// unsubscribe listeners
		subs.forEach(s=>s());
		// clear effects
		clear();
	}
};

/**
 * use memo hook
 * same as use_effect but also returns a value
 * NOTE: If you do not want to update a render value consider using use_effect
 * NOTE: You should not switch the holder type after initilisation, if you need object support use an object as the default value
 * @param {Function} callback - Callback function should return the value
 * @param {Array} dependencies - Dependency Array (see use_state or use_complex_state .bind())
 * @returns {[Object, Function, Function]} Object: inherited from use_state(or use_complex_state), Function: inherited from use_state (or use_complex_state), Function: unsubscribe from listeners
 */
export const use_memo = (callback=()=>{}, dependencies=[]) => {
	let initial = callback();
	let holder = null;

	if(typeof initial === 'object' && !Array.isArray(initial)) {
		holder = use_complex_state(initial);
	} else {
		holder = use_state(initial);
	}

	let subs = dependencies.map(dep=>dep(()=>{
		let n = callback();
		holder[1](()=>n);
	}));

	return [holder[0], holder[1], () => {
		subs.forEach(s=>s());
	}];
};

/**
 * Wrapper around Stack
 * @param {any} initial
 * @constructor
 */
export const use_ref = initial => {
	let holder = new Stack(initial);
	return holder;
}
