import _ from '../@core/dom.js';
import {StyleSheet} from '../@core/css.js';

export const Container = _.Component.define('nb-basic-container', class extends _.Component {
  constructor() {
    super();
    let el = document.createElement('div');
    this._target = el;
    this._shadow.append(el);
  }

});

export const Button = (()=>{
  let f = _.Component.define('nb-basic-button', class extends _.Component {
    constructor(){
      super();
      let el = document.createElement('button');
      this._target = el;
      this._shadow.append(el);
    };
    set onclick(cb) {
      this._target.onclick=cb;
    }
    click() {
      this._target.click();
    }
  });
  return (props={}) => f({
    ...props,
    style: {
      border: 'none',
      padding: '0.6rem 1rem',
      'font-size':'1.1rem',
      margin: '0.4rem',
      'border-radius': '12px',
      background: 'transparent',
      cursor: 'pointer',
      ...props.style,
    }
  })
})();

export const Text = (()=>{
  let f = _.Component.define('nb-basic-text', class extends _.Component {
    constructor() {
      super();
      let el = document.createElement('p');
      this._target = el;
      this._shadow.append(el);
    }
    set innerHTML(i) {
      this._target.innerHTML=i;
    }
  });
  return (txt='', props={}) => f({...props, innerHTML:txt})
})()

export const EditText = (()=>{
  let f = _.Component.define('nb-basic-input', class extends _.Component {
    constructor() {
      super();
      let el = document.createElement('input');
      this._target = el;
      this._shadow.append(el);
    }
    set type(t) {
      this._target.type=t;
    }
    set value(v) {
      this._target.value;
    }
    get value() {
      this._target.value
    }
    set placeholder(p) {
      this._target.placeholder=p
    }
    set onchange(cb){
      this._target.onchange=cb;
    }
    set onclick(cb) {
      this._target.onclick=cb
    }
    click() {
      this._target.click()
    }
  });
  return (props={}) => f({
    ...props,
    style:{
      'border-radius': '12px',
      border: 'none',
      cursor: 'pointer',
      ...props.style,
    }
  });
})();

export const HorizontalLayout = (props={}) => Container({
  ...props, 
  style:{
    ...props.style,
    'flex-direction':'row',
    display: 'flex'
  }
});
export const VerticalLayout = (props={}) => Container({
  ...props, 
  style:{
    ...props.style,
    display: 'flex',
    'flex-direction':'column'
  }
});

export const OutlinedButton = (props={}) => Button({
  ...props,
  style: {
    'border-radius': '12px',
    border: `2px solid ${props.outline || 'var(--noodlebox-outlined-button-outline)'}`,
    background: 'transparent',
    ...props.style,
  }
});

export const IconButton = (props={}) => Button({
  ...props,
  children: [props.icon],
  style: {
    'aspect-ratio': '1/1',
    'border-radius': props.radius||'25px',
    ...props.button_style,
    ...props.style,
  }
})
export const FilledIconButton = (props={}) => IconButton({
  ...props,
  style: {
    padding: props.padding || '0.2rem',
    'border-radius':props.radius||'25px',
    background: props.background || 'var(--noodlebox-filled-icon-button-background)',
    ...props.style,
  }
});

export const Icon = (()=>{
  let f = _.Component.define('nb-basic-icon', class extends _.Component {
    constructor() {
      super();
      let el = document.createElement('i');
      this._target = el;
      this._shadow.append(el);
    }
  })
  return (clipPath='circle(90%)', props={}) => f({
    ...props,
    style: {
      ...props.style,
      'clip-path': clipPath
    }
  })
})()

export const Image = (()=>{
  let f = _.Component.define('nb-basic-image', class extends _.Component {
    constructor() {
      super();
      let el = document.createElement('img');
      this._target = el;
      this._shadow.append(el);
    }
    set src(s) {
      this._target.src=s;
    }
  })
  return (src='', props={}) => f({
    ...props,
    src:props.src||src
  })
})()
