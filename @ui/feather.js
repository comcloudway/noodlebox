import _ from '../@core/dom.js';
import {StyleSheet} from '../@core/css.js';

const FeatherIcon = _.Component.define('nb-feather-icon', class extends _.Component {
    constructor() {
      super();
      let el = document.createElement('i');
      this._target = el;
      this._shadow.append(el);
    }
  });
export const FeatherIcons = (path='') => (()=>{
  return (icon='', props={}) => FeatherIcon({
    ...props,
    style:{
      width: '1.4rem',
      height: '1.4rem',
      mask: `url(${path+icon+'.svg'||props.path}) no-repeat`,
      'mask-repeat': 'no-repeat',
      'mask-image': `url(${path+icon+'.svg'||props.path})`,
      'mask-position':'center',
      'mask-size': 'cover',
      // webkit support
      '-webkit-mask-repeat': 'no-repeat',
      '-webkit-mask-image': `url(${path+icon+'.svg'||props.path})`,
      '-webkit-mask-position':'center',
      '-webkit-mask-size': 'cover',
      display: 'block',
      background: props.color || 'var(--noodlebox-feather-icon-color)',
      ...props.style,
    }
  })
})()
