# NoodleBox
## About
![Official Icon](./icon.png)

NoodleBox is the successor of the hexagonal UI Library.

Most core utils stayed the same, others were modified to some extend.

## Differences between NoodleBox and Hexagonal
### States
In Contrast to Hexagonal, NoodleBox States now only return an array with a length of 2.
Whereas the first item is an object containing the current getter, the bind() function and the subscribe() function.
(NOTE: bind was previously called from)
### DOM
The Layout of the DOM Functions were altered and are now hidden behind the _ reference.
Also Element creation is now simplified into a single object and the @core module already contains the most important elements.

# API
Javascript Functions are documented with JSDoc.
This means that informing yourself about undocumented pieces of software (those where we haven't come around to putting them in the guide) is as easy as checking the javascript code and taking a look at the JSDoc comments.

# Guide
The Guide is currently WIP.
As only some of the chapters has been written, some small text snipppets have beeen added to point the author in the right direction
## @core
NoodleBox is split into multiple modules, where as each module folder represents a new category.
The @core module contains the basic NoodleBox engine, which can be used to craft more modules.
### Getting Started
As of writing (29.09.2021) this project is not available as a node module, this means that you have to manually download the source code to work with it.

The easiest way to get started is by using git to clone the repository: `git clone https://codeberg.org/comcloudway/noodlebox`
**NOTE: On many linux distributions git comes preinstalled, if you don't know how to install it, there are many great tutorials out there, just [search for it](https://search.brave.com/search?q=install+git)**

After cloning the repository you can import the required files.
### Understanding the Element Tree
Getting comfortable with the Function and Callback based Element Tree can be quite challanging at the start.
#### Importing the required DOM module
Before we create our first Element, lets look at how we can import the DOM module.
All of the neccessary functions are contained within the _ namespace.
```javascript
import _ from 'noodlebox/@core/dom.js'
```

#### Creating a Component
Although creating elements the "normal" way (document.createElement) works perfectly fine with noodlebox, using custom components provides a way better experience

#### Using the NoodleBox wrapper
To be able to automatically update elements, wrapping them with a function is neccessary.


### Understanding States
NoodleBox States are handled similiar to React states, but with slight adaptions to keep the library as small as possible.

Every state returns an object containig a `current`-getter, a `bind` function and a `set` function.
The getter returns the current value, the bind function is used in [dependency-arrays]() and the set functions is used to update the value.
**NOTE: The set function takes a callback (we suggest the use of arrow functions). This function can take one parameter, which represents the current value of the state. After executing the callback, the states' value will be updated to represent the value returned by the callback**
#### Basic State
A basic state is a state that only holds one value, of one type.

**NOTE: Objects and Arrays can be used but might not work as expected, you are better of using a Complex state**

##### .current
##### .bind()
##### .set(...)

#### Complex State
A complex state is designed to take an object.

##### .current
##### .bind(...)
##### .set(...)

### Updating Nodes when a value changes
To know if and when a value changes you create a `bind`ing to the value.
These bindings are stored in the dependency array.
### Complex Hooks
Now that you know how to update your website with NoodleBox, see how you could improve your design by using more complex hooks
#### Effects
#### Memo
### CSS helper functions
NoodleBox's core module includes some CSS helper functions to make styling your elements from JS easier.
#### json2css
The json2css function allows you to style a HTML element by using json / javascript objects.

To use it, you have to import the json2css function from the @ui module.
```javascript
import { json2css } from 'noodlebox/@core/css.js';
```

Now styling an element is as easy as running the json2css function with the element as the first argument and the json code as the second.
```javascript
let demo = document.getElementById('demo');
json2css(element, {
  background: 'red'
});
```
#### StyleSheet
In contrast to the json2css function, creating a stylesheet does not automatically apply the styling to an element.
It creates a style-element and builds the css code from your input, it then retuns an object containing helper functions to read options.
**NOTE: When using the NoodleBox Component, a stylesheets setter can be implemented allowing you to add StyleSheets**

A new StyleSheet can be created by calling the StyleSheet function after importing it.
```javascript
import { StyleSheet } from 'noodlebox/@core/css.js';
```

To enable you to use all sort of CSS options, the function takes multiple arguments
```javascript
StyleSheet(style, custom_css_selector, custom_class_name, custom_css_template, parent)
```

##### Adding style options
In contrast to [json2css](), StyleSheet doesn't use CamelCase for its identifiers, instead every key is directly translated into CSS.
So `border-radius` stays the same, this way you can use all of your existing CSS code.
##### Using a custom css selector
Styling a plain element is somewhat boring and without hover animations a page feels less dynamic - doesn't it?
That is why you can use custom selectors.
Whilst you would be able to pull all of this off using [a custom css template](), having it be a single parameter makes the whole process a lot easier.
Taking a look at the default implementation (`{}`) we can see that `{}` is used as a placeholder for the classname.

Changing the Selector to `{}:hover` allows you to style the element in its :hover-ed state.
This can be changed to every CSS selector you want.
##### Using a custom class name
Sometimes you might want to use the same StyleSheet for multiple elements. 
In this case you have two options: 
  - You either load a css file with your stylesheet and specify the classname for the elements or
  - you create a StyleSheet with a custom class (e.g `beautiful-text`) and specify this class. This way you wont have to load a css file, it also allows you to dynamically update the elements tyle

##### Using a custom css template
Implementing animations might seem impossible at this point, but that is exactly what the custom_css_template is used for.
The default implementation (`{SELECTOR} {{CSS}}`), shows the two available variables.
`{SELECTOR}` is the classname (and [custom selector]()), which is either auto-generated or specified by the user ([Using a custom css class](#using-a-custom-class-name)).
**NOTE: These variables can be used any number of times. In fact there is no need to include them at all**

Templates allow you to build more advanced StyleSheets.
For example, implementing an animation is easy, just modify your template to look the following:
`@keyframes <animation-name> {<your normal animation code>} {SELECTOR} {{CSS}}`.
**NOTE: the text inside the arrows(<,>) are used as placeholders, you have to replace them to fit your needs**
##### Why choosing a different parent might be import
NoodleBox is build around custom html elements and shadow roots.
A style element specified in the body wont affect elements in a shadow root, that is why one has to specify the shadow root as a parent.
This technique can also be combined with custom class names, to use the same classname in different contexts'

## @ui
The 'real' UI Library.
Contains many pre-made & designed UI Components for your website
### basics
The basics library contains basic components, like TextBoxes, Containers and Images
### feather
The feather library contains bindings to the feather icon pack.
#### Getting started
To be able to use the feather icon pack you have to download the required icons (or all) and place them in a folder accessible by the client.
There are no icons shipped with this library, to keep the bundle size smaller and force people to obtain the icons from the official source.
If you are unsure of where to obtain the icons,
you can check out the [Official Github Repo](https://github.com/feathericons/feather) or their [official webpage](https://feathericons.com/).
#### Setup
To be able to display the icons, you have to initialize the FeatherIcons library after you imported it.
```javascript
import {FeatherIcons} from 'noodlebox/@ui/feather.js';
```
To do this call the FeatherIcons function you just imported an pass the path to the icon pack.
```javascript
const FeatherIcon = FeatherIcons(path);
```
#### Displaying an icon
Now you can embed you icon by calling the FeatherIcon Function and using the icon's file name (without the extension) as the first argument.
```javascript
FeatherIcon('person', {
  color: 'red'
})
```

# Shoutouts
  - HTML and CSS Icons from Wikimedia 
